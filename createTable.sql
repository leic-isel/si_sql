CREATE TABLE CAMPO_FERIAS.dbo.CAMPOFERIAS (
	codigo nvarchar(25) PRIMARY KEY,
	nome varchar(125),
	endereco varchar(125),
	localidade varchar(100),
	codpostal varchar(50),
	enderecoweb varchar(150), 
	latitude decimal(7,5),
	longitude decimal(7,5),
	UNIQUE(enderecoweb),
	CONSTRAINT CHECK_COD CHECK (codigo LIKE 'CF[0-9][0-9][0-9][0-9]'),
	CONSTRAINT CHECK_CP CHECK (codpostal LIKE '[0-9][0-9][0-9][0-9][-][0-9][0-9][0-9]'),
	CONSTRAINT CHECK_URL CHECK (enderecoweb LIKE 'www.%.pt' OR enderecoweb LIKE 'www.%.com' OR enderecoweb LIKE 'www.%.eu')
);


CREATE TABLE CAMPO_FERIAS.dbo.GRUPO (
	nome varchar(25) PRIMARY KEY,
	idademinima int CHECK (idademinima > 5),
	idademaxima int,
	CONSTRAINT CHK_AGE CHECK (idademaxima > idademinima AND idademaxima < 18),
	CONSTRAINT CHK_NAME CHECK (nome LIKE 'iniciados' OR nome LIKE 'juniores' OR nome LIKE 'seniores')
);


CREATE TABLE CAMPO_FERIAS.dbo.PESSOA (
	numero int IDENTITY (1,1) PRIMARY KEY,
	nome nvarchar(150),
	endereco nvarchar(250),
	ntelefone nvarchar(50),
	email char(150),		
	UNIQUE (ntelefone, email),
	CONSTRAINT CHK_ADDR CHECK (endereco LIKE '%[0-9][0-9][0-9][0-9][-][0-9][0-9][0-9]%'),
	CONSTRAINT CHK_PHONE CHECK (ntelefone LIKE '+351[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	CONSTRAINT CHK_EMAIL CHECK (email LIKE '%@%')
);


CREATE TABLE CAMPO_FERIAS.dbo.MONITOR (
	numero int PRIMARY KEY,
	idade int,
	escolaridade nvarchar(20),
	comonitor int,
	cpferias nvarchar(25),
	FOREIGN KEY (numero) REFERENCES PESSOA (numero),
	FOREIGN KEY (numero) REFERENCES MONITOR (numero),
	FOREIGN KEY (cpferias) REFERENCES CAMPOFERIAS (codigo),
	CONSTRAINT CHK_SCHOOL CHECK (escolaridade LIKE N'secund�rio' OR escolaridade LIKE 'superior')
);


CREATE TABLE CAMPO_FERIAS.dbo.EQUIPA (
	numero int IDENTITY (1,1) PRIMARY KEY,
	grupo varchar(25),
	designacao nvarchar(150),
	monitor int,
	FOREIGN KEY (grupo) REFERENCES GRUPO (nome),
	FOREIGN KEY (monitor) REFERENCES MONITOR (numero)
);

CREATE TABLE CAMPO_FERIAS.dbo.COLONO (
	numero int IDENTITY (1,1) PRIMARY KEY,
	nome nvarchar(150),
	dtnascimento date NOT NULL,
	contacto nvarchar(50),
	escolaridade int CHECK ( (escolaridade >= 1) AND (escolaridade <= 12)),
	ccidadao varchar(15),
	cutente decimal(10,0),
	eeducacao int,
	equipa int,
	FOREIGN KEY (eeducacao) REFERENCES PESSOA (numero),
	FOREIGN KEY (equipa) REFERENCES EQUIPA (numero),
	CONSTRAINT CHK_AGE_COL CHECK (DATEDIFF(YEAR , dtnascimento, GETDATE()) > 5 AND
	                              DATEDIFF(YEAR , dtnascimento, GETDATE()) < 18),
	CONSTRAINT CHK_CONT CHECK (contacto LIKE '+351[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')
);


CREATE TABLE CAMPO_FERIAS.dbo.CONTACTO (
	identificador nvarchar(25),
	contacto nvarchar(50),
	descricao char(50),
	PRIMARY KEY (identificador, contacto),
	FOREIGN KEY (identificador) REFERENCES CAMPOFERIAS (codigo),
	CONSTRAINT CHK_CONT2 CHECK (contacto LIKE '+351[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' OR
	                           contacto LIKE '%@%'),
	CONSTRAINT CHK_DESC CHECK (descricao LIKE 'fixo' OR descricao LIKE N'm�vel' OR descricao LIKE 'email' )
);

CREATE TABLE CAMPO_FERIAS.dbo.ACTIVIDADE (
	referencia int IDENTITY (1,1) PRIMARY KEY,
	designacao nvarchar(150),
	descricao nvarchar(150),
	duracao int CHECK ( (duracao > 0) AND (duracao < 240) ),
	participacao nvarchar(15),
	CONSTRAINT CHK_DESG CHECK (designacao LIKE 'desportiva' OR designacao LIKE 'recreativa' OR
							   designacao LIKE 'aventura' OR designacao LIKE 'desportos radicais' OR 
							   designacao LIKE 'ambientais' OR designacao LIKE 'artes' OR 
							   designacao LIKE N'educa��o'),
    CONSTRAINT CHK_PART CHECK (participacao LIKE 'opcional' OR participacao LIKE N'obrigat�rio')
);

CREATE TABLE CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (
	referencia int PRIMARY KEY,
	participantes int,
	FOREIGN KEY (referencia) REFERENCES ACTIVIDADE (referencia)
);

CREATE TABLE CAMPO_FERIAS.dbo.ACTIVIDADE_EQUIPA (
	referencia int,
	equipa int,
	horainicial time,		
	horafinal time,
	PRIMARY KEY (referencia, equipa),
	FOREIGN KEY (referencia) REFERENCES ACTIVIDADE (referencia),
	FOREIGN KEY (equipa) REFERENCES EQUIPA (numero),
	CONSTRAINT CHK_HF1 CHECK (horafinal > horainicial),
	CONSTRAINT CHK_HF2 CHECK (DATEDIFF(MINUTE, horainicial, horafinal) <= 240)
);


CREATE TABLE CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR (
	referencia int,
	monitor int,
	equipa int,
	PRIMARY KEY (referencia, monitor, equipa),
	FOREIGN KEY (referencia) REFERENCES ACTIVIDADE (referencia),
	FOREIGN KEY (monitor) REFERENCES MONITOR (numero),
	FOREIGN KEY (equipa) REFERENCES EQUIPA (numero)
);

CREATE TABLE CAMPO_FERIAS.dbo.REPRESENTANTE (
	colono int,
	equipa int,
	PRIMARY KEY (colono, equipa),
	FOREIGN KEY (colono) REFERENCES COLONO (numero),
	FOREIGN KEY (equipa) REFERENCES EQUIPA (numero)
);
