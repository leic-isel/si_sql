use CAMPO_FERIAS;

-- 3 a) i) #############################################################################################################
SELECT nome, EQUIPA.designacao FROM PESSOA
    JOIN EQUIPA
        ON EQUIPA.numero = PESSOA.numero;


-- 3 a) ii) ############################################################################################################
SELECT nome, 'colono' Funcao FROM COLONO
    JOIN ACTIVIDADE_EQUIPA
        ON COLONO.equipa = ACTIVIDADE_EQUIPA.equipa
    JOIN ACTIVIDADE
        ON ACTIVIDADE_EQUIPA.referencia = ACTIVIDADE.referencia
WHERE ACTIVIDADE.designacao = 'ambientais'

UNION

SELECT PESSOA.nome, 'monitor' Funcao from MONITOR
    JOIN PESSOA
        ON MONITOR.numero = PESSOA.numero
    JOIN ACTIVIDADE_MONITOR
        ON ACTIVIDADE_MONITOR.monitor = MONITOR.numero
    JOIN ACTIVIDADE
        ON ACTIVIDADE.referencia = ACTIVIDADE_MONITOR.referencia
WHERE ACTIVIDADE.designacao = 'ambientais';


-- 3 a) iii) ###########################################################################################################
SELECT ACTIVIDADE.designacao FROM ACTIVIDADE
    JOIN ACTIVIDADE_DESPORTIVA
        ON ACTIVIDADE.referencia = ACTIVIDADE_DESPORTIVA.referencia
WHERE ACTIVIDADE.participacao = N'obrigatório' AND ACTIVIDADE_DESPORTIVA.participantes >= 5;


-- 3 a) iv) ############################################################################################################
SELECT EQUIPA.designacao FROM EQUIPA
    JOIN ACTIVIDADE_EQUIPA
        ON EQUIPA.numero = ACTIVIDADE_EQUIPA.equipa
    JOIN ACTIVIDADE
        ON ACTIVIDADE_EQUIPA.referencia = ACTIVIDADE.referencia
WHERE ACTIVIDADE.designacao <> 'desportos radicais';


-- 3 a) v) #############################################################################################################
SELECT PESSOA.numero, PESSOA.nome, MONITOR.idade FROM PESSOA
    JOIN MONITOR
        ON MONITOR.numero = PESSOA.numero
    LEFT JOIN ACTIVIDADE_MONITOR
        ON ACTIVIDADE_MONITOR.monitor = MONITOR.numero
WHERE ISNULL(ACTIVIDADE_MONITOR.referencia, 0) = 0;


-- 3 a) vi) ############################################################################################################
SELECT descricao FROM ACTIVIDADE
    JOIN ACTIVIDADE_EQUIPA
        ON ACTIVIDADE_EQUIPA.referencia = ACTIVIDADE.referencia
    JOIN EQUIPA
        ON ACTIVIDADE_EQUIPA.equipa = EQUIPA.numero
WHERE EQUIPA.grupo = 'iniciados';


-- 3 a) vii) ###########################################################################################################
SELECT nome FROM COLONO
    JOIN REPRESENTANTE
        ON REPRESENTANTE.colono = COLONO.numero
    JOIN EQUIPA
        ON EQUIPA.numero = COLONO.equipa
WHERE (
    SELECT COUNT(ALL COLONO.equipa) FROM COLONO
    WHERE EQUIPA.numero = COLONO.equipa
    ) > 4;


-- 3 a) viii) ##########################################################################################################
SELECT AVG(DATEDIFF(year, dtnascimento ,GETDATE())) average, e.designacao, e.grupo FROM COLONO c, EQUIPA e
WHERE e.numero = c.equipa
GROUP BY c.equipa, e.designacao, e.grupo;


-- 3 b) ################################################################################################################
SELECT nome FROM COLONO
ORDER BY dtnascimento;


-- 3 c) ################################################################################################################
SELECT PESSOA.nome, endereco FROM PESSOA
    JOIN COLONO C
        ON PESSOA.numero = C.eeducacao
GROUP BY PESSOA.nome, PESSOA.numero, endereco
HAVING COUNT(*) > 1;


-- 3 d) ################################################################################################################
SELECT ACTIVIDADE.designacao Actividade, DATEDIFF(HOUR , horainicial, horafinal) Duracao FROM ACTIVIDADE
    JOIN ACTIVIDADE_EQUIPA
        ON ACTIVIDADE.referencia = ACTIVIDADE_EQUIPA.referencia
ORDER BY Duracao DESC;


-- 3 e) ################################################################################################################
SELECT designacao FROM ACTIVIDADE
    LEFT JOIN ACTIVIDADE_EQUIPA
        ON ACTIVIDADE.referencia = ACTIVIDADE_EQUIPA.referencia
EXCEPT
    (SELECT designacao FROM ACTIVIDADE
        JOIN ACTIVIDADE_EQUIPA
            ON ACTIVIDADE.referencia = ACTIVIDADE_EQUIPA.referencia
    WHERE horainicial = '12:00:00' AND horafinal = '14:00:00');


-- 3 f) ################################################################################################################
SELECT nome FROM PESSOA
    JOIN MONITOR
        ON PESSOA.numero = MONITOR.comonitor
GROUP BY PESSOA.nome
HAVING MAX(DISTINCT MONITOR.comonitor) > 0


-- 3 g) ################################################################################################################
CREATE VIEW ACTIVIDADES_A_DECORRER AS
    SELECT ACTIVIDADE.designacao Actividade,
           EQUIPA.designacao Equipa,
           COUNT(COLONO.equipa) COLONOS,
           ACTIVIDADE_EQUIPA.horainicial FROM ACTIVIDADE
        JOIN ACTIVIDADE_EQUIPA
            ON ACTIVIDADE.referencia = ACTIVIDADE_EQUIPA.referencia
        JOIN EQUIPA
            ON ACTIVIDADE_EQUIPA.equipa = EQUIPA.numero
        JOIN COLONO
            ON EQUIPA.numero = COLONO.equipa
    WHERE ACTIVIDADE_EQUIPA.horafinal > CONVERT(TIME, GETDATE())
    GROUP BY ACTIVIDADE.designacao, EQUIPA.designacao, ACTIVIDADE_EQUIPA.horainicial;

-- SELECT * FROM ACTIVIDADES_A_DECORRER; -- DEBUG LINE TO TEST THE VIEW


-- 4 ###################################################################################################################
INSERT INTO COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa)
VALUES ('Manuel Henrique', '21/11/2012', '+351943204674', 3, '36312151 0ZZ4', '123456789', 2, 1);