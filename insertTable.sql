BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.DBO.CAMPOFERIAS (codigo, nome, endereco, localidade, codpostal, enderecoweb, latitude, longitude) VALUES ('CF0001', 'Os hulks', 'Rua de cima', 'lx', '1000-100', 'www.hulks.pt', 45.54321, -17.09736);
        INSERT INTO CAMPO_FERIAS.DBO.CAMPOFERIAS (codigo, nome, endereco, localidade, codpostal, enderecoweb, latitude, longitude) VALUES ('CF0002', 'As laranjas', 'Rua de baixo', 'CB', '2000-100', 'www.laranjas.pt', 45.55321, -19.09736);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH



BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.GRUPO (nome, idademinima, idademaxima) VALUES ('iniciados', 6, 11);
        INSERT INTO CAMPO_FERIAS.dbo.GRUPO (nome, idademinima, idademaxima) VALUES ('juniores', 12, 15);
        INSERT INTO CAMPO_FERIAS.dbo.GRUPO (nome, idademinima, idademaxima) VALUES ('seniores', 16, 17);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH


BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES (N'F�bio Teixeira',
                                                                                       N'R. J�io Dinis, lote 506, 1685-164, Fam�es', '+351963692624', 'fabioteixeirahl@gmail.com');
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES ('Nuno Venancio', 'R. Banco, 1000-100, lx', '+351930001122', 'asdf@gmail.com');
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES ('Helder Augusto', 'R. Preto, 1032-105', '+351929990011', 'ha@gmail.com');
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES ('Maria', 'R. Rosa, 1023-555', '+351964434343', 'mj@gmail.com');
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES ('Joao Teixeira', 'R. laranja, 1078-987', '+351966534343', 'jt@gmail.com');
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES ('Nuno', 'R. Roxo, 1520-252', '+351964333343', 'nt@gmail.com');
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES ('Patricia', 'R. Amarelo, 1900-202,', '+351964111343', 'pb@gmail.com');
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES (N'Ant�nio', 'R. Azul, 1800-654', '+351961234343', 'as@gmail.com');
        INSERT INTO CAMPO_FERIAS.dbo.PESSOA (nome, endereco, ntelefone, email) VALUES ('Joana', 'Estrada da Luz, 1200-250', '+351915488751', 'jj@outlook.pt');
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH


BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.MONITOR (numero, idade, escolaridade, cpferias) VALUES (1 ,'30', N'secund�rio', 'CF0001' );
        INSERT INTO CAMPO_FERIAS.dbo.MONITOR (numero, idade, escolaridade, comonitor, cpferias) VALUES (2, '32', 'superior', 1, 'CF0002' );
        INSERT INTO CAMPO_FERIAS.dbo.MONITOR (numero, idade, escolaridade, comonitor, cpferias) VALUES (4, '45', 'superior', 1, 'CF0001' );
        INSERT INTO CAMPO_FERIAS.dbo.MONITOR (numero, idade, escolaridade, comonitor, cpferias) VALUES (5, '39', 'superior', 1, 'CF0001' );
        INSERT INTO CAMPO_FERIAS.dbo.MONITOR (numero, idade, escolaridade, comonitor, cpferias) VALUES (7, '23', 'superior', 1, 'CF0001' );
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH


BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.EQUIPA (grupo, designacao, monitor) VALUES ('iniciados', 'equipaA', 1);
        INSERT INTO CAMPO_FERIAS.dbo.EQUIPA (grupo, designacao, monitor) VALUES ('juniores', 'equipaB', 2);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH


BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES (N'Tom�s', '03/07/2014', '+351966666666', 1, '123123321', 2213213, 1, 1);
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES (N'Andr�', '03/13/2014', '+351966666661', 1, '123123123', 2213456, 4, 2);
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES ('Hugo', '06/29/2010', '+351966666662', 1, '123123432', 2213416, 2, 1);
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES (N'Jo�o', '05/16/2011', '+351966666663', 1, '123123456', 2213426, 3, 2);
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES ('Ricardo', '12/11/2009', '+351966666664', 1, '123123878', 2213656, 5, 1);
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES ('Santiago', '01/15/2012', '+351966666665', 1, '123123979', 2213876, 6, 2);
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES ('Samuel', '01/13/2010', '+351966666632', 1, '123123272', 2213986, 7, 1);
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES (N'S�rgio', '02/26/2013', '+351966666634', 1, '123123876', 2213996, 8, 2);
        INSERT INTO CAMPO_FERIAS.dbo.COLONO (nome, dtnascimento, contacto, escolaridade, ccidadao, cutente, eeducacao, equipa) VALUES ('Duarte', '06/07/2014', '+351123466341', 3, '12316685', 2213996, 8, 1);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH



BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.CONTACTO (identificador, contacto, descricao) VALUES ('CF0001', '+351213230000', 'fixo');
        INSERT INTO CAMPO_FERIAS.dbo.CONTACTO (identificador, contacto, descricao) VALUES ('CF0001', 'oshulks@gmail.com', 'email');
        INSERT INTO CAMPO_FERIAS.dbo.CONTACTO (identificador, contacto, descricao) VALUES ('CF0002', '+351215550001', 'fixo');
        INSERT INTO CAMPO_FERIAS.dbo.CONTACTO (identificador, contacto, descricao) VALUES ('CF0002', 'aslaranjas@gmail.com', 'email');
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH


BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE (designacao, descricao, duracao, participacao) VALUES ('desportiva', 'Andar de mota', 239, N'obrigat�rio');
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE (designacao, descricao, duracao, participacao) VALUES ('recreativa', 'Fazer um capacete em papel', 60, N'obrigat�rio');
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE (designacao, descricao, duracao, participacao) VALUES ('aventura', 'Pedipaper', 120, 'opcional');
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE (designacao, descricao, duracao, participacao) VALUES ('desportos radicais', 'Fazer escalada', 239, N'obrigat�rio');
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE (designacao, descricao, duracao, participacao) VALUES ('ambientais', N'Horta comuni�tia', 239, N'obrigat�rio');
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE (designacao, descricao, duracao, participacao) VALUES ('artes', 'Fazer escultura', 30, 'opcional');
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE (designacao, descricao, duracao, participacao) VALUES (N'educa��o', 'Ler um livro', 60, 'opcional');
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH



BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (referencia, participantes) VALUES (1, 2);
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (referencia, participantes) VALUES (4, 4);
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (referencia, participantes) VALUES (5, 5);
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (referencia, participantes) VALUES (2, 2);
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (referencia, participantes) VALUES (3, 6);
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (referencia, participantes) VALUES (6, 7);
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_DESPORTIVA (referencia, participantes) VALUES (7, 4);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH



BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_EQUIPA (referencia, equipa, horainicial, horafinal) VALUES (1, 1, '08:00', '09:00');
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_EQUIPA (referencia, equipa, horainicial, horafinal) VALUES (4, 2, '08:00', '09:00');
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_EQUIPA (referencia, equipa, horainicial, horafinal) VALUES (5, 1, '12:00', '14:00');
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH


BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR (referencia, monitor, equipa) VALUES (1, 1, 1);
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR (referencia, monitor, equipa) VALUES (4, 2, 2);
        INSERT INTO CAMPO_FERIAS.dbo.ACTIVIDADE_MONITOR (referencia, monitor, equipa) VALUES (5, 4, 2);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH


BEGIN TRY
    BEGIN TRANSACTION
        INSERT INTO CAMPO_FERIAS.dbo.REPRESENTANTE (colono, equipa) VALUES (1, 1);
        INSERT INTO CAMPO_FERIAS.dbo.REPRESENTANTE (colono, equipa) VALUES (2, 2);
    COMMIT TRANSACTION
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
END CATCH